<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Session;

use App\Category;
use App\sub_categorie;

class customerLoginController extends Controller
{
    public function __construct(){
        $this->middleware('guest:customer');
    }
    public function showLoginForm(){

     $category=Category::all();
    $subCategory=sub_categorie::all();

        return view('frontends.login')

    ->with('category',$category)
    ->with('subCategory',$subCategory);
    }
    public function login(Request $request){
        //validate the form data
        $this->validate($request,[
            'email'=>'required|email',
            'password'=>'required|min:6'
        ]);
    //Attem Login
        if (Auth::guard('customer')->attempt(['email' => $request->email, 'password' => $request ->password], $request->remember)) {
            // if success then redirect to their intended location
            Session()->put('user_name', $request->email);
            return redirect()->intended(route('welcome'));
        }
        //if unsuccess then back login form
        return redirect()->back()->withInput($request->only('email','remember'));
        
    }
}
