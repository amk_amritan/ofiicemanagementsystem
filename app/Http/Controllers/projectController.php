<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\project;

class projectController extends Controller
{
    public function index()
    {
    	$project=project::all();
    	return view('admins.project.index')->with('project', $project);
    }
    public function create()
    {
    	return view('admins.project.create');
    }
    public function store(Request $request)
    {
    	$projects =new project;

    	$projects->name=$request->name;
    	$projects->price=$request->price;
    	$projects->note=$request->note;
    	$projects->status=$request->status;
    	$projects->project_id=$request->project_id;

    	$projects->save();
    	$project=project::all();

    	return  view('admins.project.index')->with('project', $project);
    }	
    public function edit($id)
    {
    	$project = project::find($id);
    	return view('admins.project.edit')->with('project', $project);
    }
    public function update(Request $request, $id)
    {
    	$projects = project::find($id);
        
        $projects->name = $request->input('name');
        $projects->price = $request->input('price');
        $projects->note = $request->input('note');
        $projects->status = $request->input('status');
        $projects->project_id = $request->input('project_id');

         $projects->save();	
         $project = project::all();
    	return  view('admins.project.index')->with('project', $project);
    }
}
