<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Bookupload;
use App\Blog;
use App\Category;
use App\sub_categorie;
use DB;
use App\Service;
use App\Slider;


class IndexController extends Controller
{
    public function index(){

    $product = DB::table('products')->paginate(8);
	$Cproduct =Product::where('categories_id', '=', 3)->paginate(2);
	$Coproduct =Product::where('categories_id', '=', 2)->paginate(2);
	$Co4product =Product::where('categories_id', '=', 5)->paginate(2);
	$Co5product =Product::where('categories_id', '=', 4)->paginate(2);
    $service=DB::table('services')->paginate(10);
    $slider= Slider::all();
    $book=DB::table('bookuploads')->paginate(4);
	// $book=Bookupload::all();
	$blog=Blog::all();
     $category=Category::all();
    $subCategory=sub_categorie::all();
    	return view('index') 
    ->with('product',$product)
    ->with('book',$book)
    ->with('blog', $blog)
    ->with('Cproduct', $Cproduct)
    ->with('Co4product',$Co4product)
    ->with('Co5product',$Co5product)
    ->with('Coproduct', $Coproduct)
    ->with('category',$category)
    ->with('service',$service)
    ->with('slider',$slider)
    ->with('subCategory',$subCategory);
    } 
    public function service(){
        $service=DB::table('services')->paginate(4);
        return view('image_carousel')->with('service', $service);
    }
}
