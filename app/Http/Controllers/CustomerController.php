<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use validator;
use App\User;
use App\Category;
use App\sub_categorie;
use Illuminate\Support\Facades\Hash;

class CustomerController extends Controller
{
    public function index(){
        $category=Category::all();
        $subCategory=sub_categorie::all();
        return view('myAccount/customerLogin')
        ->with('category',$category)
        ->with('subCategory',$subCategory);
    }
	

    public function RegsiterForm(){
    	$category=Category::all();
    	$subCategory=sub_categorie::all();

    	return view('myAccount/customerRegister')

		->with('category',$category)
    	->with('subCategory',$subCategory);
    }
    public function registerCustomer(Request $request){
    	$category=Category::all();
    	$subCategory=sub_categorie::all();

    	$user = new User;
        $user->email = $request->email;
        $user->fname = $request->fname;
        $user->lname = $request->lname;
        $user->country = $request->country;
        $user->city = $request->city;
        $user->ContactNumber = $request->ContactNumber;
        $passwords=Hash::make($request->password);
        $user->password = $passwords;
        $user->save();

    	return view('myAccount/customerLogin')

		->with('category',$category)
    	->with('subCategory',$subCategory);

    }

    public function customerInfoUpdate(Request $request){
        $category=Category::all();
        $subCategory=sub_categorie::all();

        $customerInfo=User::find(Auth::user()->id);
        $customerInfo->email=$request->input('email');
        $customerInfo->fname=$request->input('fname');
        $customerInfo->lname=$request->input('lname');
        $customerInfo->country=$request->input('country');
        $customerInfo->city=$request->input('city');
        $customerInfo->ContactNumber=$request->input('ContactNumber');


        $customerInfo->save();
        Auth::logout();

        return view('myAccount/customerLogin')

        ->with('category',$category)
        ->with('subCategory',$subCategory);

    }


    public function loginFormShow(){
    	$category=Category::all();
    	$subCategory=sub_categorie::all();
    	return view('myAccount/customerLogin')
    	->with('category',$category)
    	->with('subCategory',$subCategory);
    }

    public function checkLogin(Request $request){
    	$this->validate($request,[
    		'email'  =>'required|email',
    		'password' =>'required|alphaNum|min:3'
    	]);

    	$user_data = array(

    		'email'       =>$request->get('email'),
    		'password'		=>$request->get('password')

    	);

    	if (Auth::attempt($user_data)) {

    		return redirect('customerLogin/successLogin');
    	}
    	else{

    		return back()->with('error','Wrong Login Detail');
    	}
    }

    public function successLogin(){
        
        $userInfo=User::where('email', '=', Auth::user()->email)->get();
    	$category=Category::all();
    	$subCategory=sub_categorie::all();

    	return view('myAccount/AccountDetails')

    	->with('category',$category)
        ->with('userInfo', $userInfo)
    	->with('subCategory',$subCategory);
    }


    public function logout(){
    	$category=Category::all();
    	$subCategory=sub_categorie::all();

    	Auth::logout();

    	return view('myAccount/customerLogin')
    	->with('category',$category)
    	->with('subCategory',$subCategory);
    }


    public function personalInformation(){
        
        $id=Auth::user()->email;
        $userInfo=User::where('email', '=', $id)->get();
        $category=Category::all();
        $subCategory=sub_categorie::all();

        return view('myAccount/personalInformation')

        ->with('category',$category)
        ->with('userInfo', $userInfo)
        ->with('subCategory',$subCategory);

    }

public function ratings(){


        $category=Category::all();
        $subCategory=sub_categorie::all();
         return view('myAccount/MyRatings')

        ->with('category',$category)
        ->with('subCategory',$subCategory);
}

public function customerOrder(){


        $category=Category::all();
        $subCategory=sub_categorie::all();

         return view('myAccount/MyOrders')

        ->with('category',$category)
        ->with('subCategory',$subCategory);
}



}
