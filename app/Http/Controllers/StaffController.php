<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Staff;

class StaffController extends Controller
{
    public function index()
    {
    	$staff=Staff::all();
    	return view('admins.staff.index')->with('staff', $staff);
    }
    public function create()
    {
    	return view('admins.staff.cerate');
    }
    public function store(Request $request)
    {
    	$staff= new Staff;
    	$staff->name=$request->name;
    	$staff->email=$request->email;
    	$staff->temporary_address=$request->temporary_address;
    	$staff->parement_address=$request->parement_address;
    	$staff->post=$request->post;
    	$staff->phone=$request->phone;

    	$staff->save();
    	$staff=Staff::all();
    	return view('admins.staff.index')->with('staff', $staff);
    }
    public function edit($id){
    	$staff=Staff::find($id);
    	return view('admins.staff.edit')->with('staffs',$staff);
    }
    public function update(Request $request, $id){

    	$staff = Staff::find($id);
        
        $staff->name = $request->input('name');
        $staff->email = $request->input('email');
        $staff->temporary_address = $request->input('temporary_address');
        $staff->parement_address = $request->input('parement_address');
        $staff->post = $request->input('post');
        $staff->phone = $request->input('phone');

        $staff->save();
        $staff=Staff::all();
        return view('admins.staff.index')->with('staff', $staff);;
    }
}
