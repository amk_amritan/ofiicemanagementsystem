<?php

namespace App\Http\Controllers;
use App\Bill;

use Illuminate\Http\Request;

class billController extends Controller
{
    public function index()
    {
    	$bill = Bill::all();

    	return view('admins/bill/index')->with('bill', $bill); 
    }
    public function create()
    {
    	return view('admins/bill/create');
    }

    public function store(Request $request){

    	$bills= new Bill;
    	$bill = Bill::all();
    	$bills->bill_name=$request->bill_name;
    	$bills->bill_number=$request->bill_number;
    	$bills->bill_amount=$request->bill_amount;
    	$bills->cr_dr=$request->cr_dr;
    	$bills->note=$request->note;

    	$bills->save();
    	return view('admins.bill.index')->with('bill',$bill);

    }
    public function edit($id)
    {
    	$bills= Bill::find($id);
    	return view('admins.bill.edit')->with('bills',$bills);

    }
    public function update(Request $request, $id)

    {
    		$bills = Bill::find($id);
        
        $bills->bill_name = $request->input('bill_name');
        $bills->bill_number = $request->input('bill_number');
        $bills->cr_dr = $request->input('cr_dr');
        $bills->bill_amount = $request->input('bill_amount');
        $bills->note = $request->input('note');

         $bills->save();	
         $bill = Bill::all();
    	return view('admins.bill.index')->with('bill',$bill);

    }
}




