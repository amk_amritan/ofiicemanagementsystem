<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/frontends', function () {
// 	$product = DB::table('products')->paginate(8);
// 	$Cproduct =Product::where('categories_id', '=', 3)->paginate(2);
// 	$Coproduct =Product::where('categories_id', '=', 2)->paginate(2);
// 	$Co4product =Product::where('categories_id', '=', 5)->paginate(2);
// 	$Co5product =Product::where('categories_id', '=', 4)->paginate(2);
// 	$book=Bookupload::all();
// 	$blog=Blog::all();
//      $category=Category::all();
//     $subCategory=sub_categorie::all();
//     return view('frontends.index')
//     ->with('product',$product)
//     ->with('book',$book)
//     ->with('blog', $blog)
//     ->with('Cproduct', $Cproduct)
//     ->with('Co4product',$Co4product)
//     ->with('Co5product',$Co5product)
//     ->with('Coproduct', $Coproduct)
//     ->with('category',$category)
//     ->with('subCategory',$subCategory);
// });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('admin')->group(function(){
	Route::get('/login','Auth\AdminLoginController@showLoginForm')->name('admin.login');
	Route::post('/login','Auth\AdminLoginController@login')->name('admin.login.submit');
	Route::get('/', 'AdminController@index')->name('admin');
});

Route::prefix('customer')->group(function(){
    Route::get('/login','Auth\CustomerLoginController@showLoginForm')->name('customer.login');
    Route::post('/login','Auth\CustomerLoginController@login')->name('customer.login.submit');
    Route::get('/', 'CustomerController@index')->name('customer');
});




Route::resource('admins/review','ReviewController');
Route::resource('admins/information','informationController');
// Route::resource('admins/product','ProductController');
// Route::resource('admins/product_detail','product_detailController');
// Route::resource('admins/featureImage','FeatureImageController');
// Route::resource('admins/Bookupload','BookuploadController');
// Route::resource('admins/OrderList','OrderListController');
// Route::resource('admins/Payment','PaymentController');
Route::resource('admins/User','UserController');
// Route::resource('admins/Blog','BlogController');
Route::resource('admins/Slider','SliderController');
Route::resource('admins/Gallary','GallaryController');
Route::resource('admins/Service','ServiceController');
Route::resource('admins/event','EventController');
Route::resource('admins/bill','billController');
Route::resource('admins/staff','StaffController');
Route::resource('admins/project','projectController');




//Page Route Start


// Route::get('/logins', function(){
//     $category=Category::all();
//     $subCategory=sub_categorie::all();
//     return view('frontends/login')
//     ->with('category',$category)
//     ->with('subCategory',$subCategory);
// });
// Route::get('/registers', function(){
//     $category=Category::all();
//     $subCategory=sub_categorie::all();
//     return view('frontends/register')
//     ->with('category',$category)
//     ->with('subCategory',$subCategory);
// });
// Route::get('/wishlist', function(){
//     $category=Category::all();
//     $subCategory=sub_categorie::all();
//     return view('frontends/wishlist')
//     ->with('category',$category)
//     ->with('subCategory',$subCategory);
// });

// Route::get('/mycart', function(){
//     $category=Category::all();
//     $subCategory=sub_categorie::all();
//     return view('frontends/mycart')
//     ->with('category',$category)
//     ->with('subCategory',$subCategory);
// });

// Route::get('/contactus', function(){
//      $category=Category::all();
//     $subCategory=sub_categorie::all();
//     return view('frontends/contactus')

//     ->with('category',$category)
//     ->with('subCategory',$subCategory);
// });
// Route::get('/aboutus', function(){
//      $category=Category::all();
//     $subCategory=sub_categorie::all();
//     return view('frontends/aboutus')

//     ->with('category',$category)
//     ->with('subCategory',$subCategory);

// });
// Route::get('/productDetail', function(){
//      $category=Category::all();
//     $subCategory=sub_categorie::all();
//     return view('frontends/productDetail')

//     ->with('category',$category)
//     ->with('subCategory',$subCategory);
// });
// Route::get('/categories/{id}', function($id){
//     $category=Category::all();
//     $subCategory=sub_categorie::all();
//     $singleCategoryproduct =Product::where('categories_id', '=', $id)->paginate(2);
//     return view('frontends.categories')
//     ->with('category',$category)
//     ->with('subCategory',$subCategory)
//     ->with('singleCategoryproduct',$singleCategoryproduct);
// });

//Page Route End

//Data pass different page start hear-->



// Route::resource('frontends.frontend_layouts.header','categoryDisplayController');
// Route::resource('frontends.header','categoryDisplaysController@index');
// Route::resource('frontends.frontend_layouts.footer','categoryFooterDisplaysController');

// Data pass different page End


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Customer Register Route 

// Route::resource('registers','customerRegisterController');

// Redirct Product Display by category page
//Route::get('/{id}','productCategoryDisplayController@displayProduct')->name('categories.displayProduct');


// Redirct Product Display by  Subcategory page
//Route::get('/{subid}','subProductCategoryDisplayController@displaySubProduct')->name('Subcategories.displaySubProduct');

// Redirct BlogDetail  by  Each blog page


// Route::resource('/blogDisplay','blogSingleDisplayController');





// New Start Hear 

Route::get('/','IndexController@index');
Route::get('/product/{id}','ProductDisplayByCategoryController@products');
Route::get('/subProduct/{id}','ProductDisplayByCategoryController@subProducts');
Route::get('/productDetail/{id}','ProductDisplayByCategoryController@productSingle');

Route::get('/blogs/','BlogDisplayController@blogDisplay');
Route::get('/blogDetail/{id}','BlogDisplayController@blogDetailDisplay');
Route::get('/service/','BlogDisplayController@service');
Route::get('/servicedetail/{id}','BlogDisplayController@serviceDetailDisplay');
//Customer Registation

Route::get('/customerRegister',[
	'uses'   =>  'CustomerController@RegsiterForm',
	'as'    =>		'customerRegister'
	
]);

Route::get('/customerRegister/registerCustomer/',[
	'uses'   =>  'CustomerController@registerCustomer',
	'as'    =>		'/customerRegister/registerCustomer'
	
]);

//Customer Login

Route::get('/customerLogin','CustomerController@loginFormShow');
Route::post('/customerLogin/checkLogin', 'CustomerController@checkLogin');
Route::get('/customerLogin/successLogin','CustomerController@successLogin');
Route::get('/customerLogin/logout','CustomerController@logout');

// Set Customer Information 

Route::get('/personalInformation','CustomerController@personalInformation');
Route::post('/customerInfoUpdate','CustomerController@customerInfoUpdate');

Route::get('/ratings','CustomerController@ratings');
Route::get('/customerOrders','CustomerController@customerOrder');

//page controller route

Route::get('/aboutUs','siteController@about');
Route::get('/termAndCondition','siteController@termAndCondition');
Route::get('/contact','siteController@contact');
Route::get('/books/','siteController@booksDisplay');
Route::get('/booksDetail/{id}','siteController@booksDetailDisplay');

Route::get('/event/','siteController@eventDisplay');
Route::get('/eventDetail/{id}','siteController@eventDetailDisplay');

Route::get('/Service/','siteController@booksDisplay');
Route::get('/servicedetail/{id}','siteController@DetailSerives');


// cart Rout is Hear ..
//Route::get('/addToCart/{id}','CartController@getAddToCart');
Route::get('/addToCart/{id}',[
	'uses'   =>  'CartController@getAddToCart',
	'as'    =>		'addToCart'
]);
Route::post('/cartUpdate/{id}',[
	'uses'   =>  'CartController@cartUpdate',
	'as'    =>		'cartUpdate'
]);

Route::get('/remove/{id}',[
	'uses'   =>  'CartController@removeItem',
	'as'    =>		'remove'
]);
Route::get('/shopping_cart',[
	'uses'   =>  'CartController@getCart',
	'as'    =>		'shopping_cart'
]);

Route::get('/checkout',[
	'uses'   =>  'CartController@getCheckout',
	'as'    =>		'checkout',
]);
Route::post('/checkout',[
	'uses'   =>  'CartController@postCheckout',
	'as'    =>		'checkout',
]);
Route::get('pay-with-paypal','CartController@withPaypal')->name('payment.paypal');
Route::get('paypalsuccess','CartController@paypalsuccess')->name('paypalsuccess');

Route::post('/search',[
	'uses'   =>  'CartController@search',
	'as'    =>		'checkout',
]);
Route::post('/review/',[
	'uses'   =>  'ProductDisplayByCategoryController@review',
	'as'    =>		'checkout',
]);





