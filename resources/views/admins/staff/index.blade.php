@extends('admins.bill.base')

@section('action-content')
<div class="span9">
					<div class="content">

						<div class="module">
							<div class="module-head">
								<h3>All Staff</h3>
								<a style="padding-left: 700px; text-decoration: none;" href="{{asset('admins/staff/create')}}">Create New Staff</a>
							</div>

							<div class="module-body">
	
				
								<table class="table table-striped">
								  <thead>
									<tr>
									  <th>#</th>
									  <th>Staff Name</th>
									  <th>Staff Post</th>
									  <th>Staff phone </th>
									  <th>Action</th>
									</tr>
								  </thead>
								  <tbody>
								  	@foreach($staff as $staffs)
									<tr>
									  <td>{{$staffs->id}}</td>
									  <td>{{$staffs->name}}</td>
									  <td>{{$staffs->post}}</td>
									  <td>{{$staffs->phone}}</td>
									  <td> <a style="text-decoration: none;" href="{{route('staff.edit',$staffs->id)}}">Edit</a>  | <a style="text-decoration: none;" href="">Delete</a> </td>
									</tr>
									@endforeach
									
								  </tbody>
								</table>

								
							</div>
						</div><!--/.module-->

					<br />
						
					</div><!--/.content-->
				</div><!--/.span9-->
			</div>
		</div><!--/.container-->
	</div><!--/.wrapper-->
	@endsection