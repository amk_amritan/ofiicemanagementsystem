@extends('admins.bill.base')

@section('action-content')
<div class="span9">
					<div class="content">

						<div class="module">
							<div class="module-head">
								<h3>Enter a Bill Detail</h3>
							</div>
							

									<!-- <form class="form-horizontal row-fluid"> -->
										{!! Form::open(['route'=>'staff.store' , 'class'=>"form-horizontal row-fluid"  ,'files'=>true]) !!}
										<div class="control-group">
											<label class="control-label" for="basicinput">Full Name</label>
											<div class="controls">
												<input type="text" id="basicinput" name="name" placeholder="Enter a bill Name" class="span8">
												
											</div>
										</div>

										

										<div class="control-group">
											<label class="control-label" for="basicinput">Email</label>
											<div class="controls">
												<input data-title="A tooltip for the input" type="text" placeholder="Enter a Bill number..." data-original-title="" class="span8 tip" name="email">
											</div>
										</div>

											<div class="control-group">
											<label class="control-label" for="basicinput">Temporary Address</label>
											<div class="controls">
												<input data-title="A tooltip for the input" type="text" placeholder="Enter a Bill number..." data-original-title="" class="span8 tip" name="temporary_address">
											</div>
										</div>

										<div class="control-group">
											<label class="control-label" for="basicinput">Parement Address</label>
											<div class="controls">
												<input data-title="A tooltip for the input" type="text" placeholder="Enter a Bill number..." data-original-title="" class="span8 tip" name="parement_address">
											</div>
										</div>


											<div class="control-group">
											<label class="control-label" for="basicinput">Post</label>
											<div class="controls">
												<input data-title="A tooltip for the input" type="text" placeholder="Enter a Bill number..." data-original-title="" class="span8 tip" name="post">
											</div>
										</div>
								

											<div class="control-group">
											<label class="control-label" for="basicinput">Phone</label>
											<div class="controls">
												<input data-title="A tooltip for the input" type="text" placeholder="Enter a Bill number..." data-original-title="" class="span8 tip" name="phone">
											</div>
										</div>
						
										

										<div class="control-group">
											<label class="control-label" for="basicinput">Note</label>
											<div class="controls">
												<textarea name="note" class="span8" rows="5"></textarea>
											</div>
										</div>

										<div class="control-group">
											<div class="controls">
								{{Form::submit('Create', array('class'=> 'btn btn-success btn-lg'))}}
												<!-- <button type="submit" class="btn">Submit Form</button> -->
											</div>
										</div>
									{!! Form::close() !!} 

							</div>
						</div>

						
						
					</div><!--/.content-->
				</div><!--/.span9-->
			</div>
		</div><!--/.container-->
	</div><!--/.wrapper-->

		@endsection
