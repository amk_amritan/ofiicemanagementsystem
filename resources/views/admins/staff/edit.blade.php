@extends('admins.bill.base')

@section('action-content')
<div class="span9">
					<div class="content">

						<div class="module">
							<div class="module-head">
								<h3>Enter a Bill Detail</h3>
							</div>
							

									{!! Form::model($staffs ,['route'=>['staff.update',$staffs->id], 'class'=>"form-horizontal row-fluid"  ,'method' => 'PUT']) !!}
										<div class="control-group">
											<label class="control-label" for="basicinput">Full Name</label>
											<div class="controls">
												<input type="text" value="{{$staffs->name}}" id="basicinput" name="name" placeholder="Enter a bill Name" class="span8">
												
											</div>
										</div>

										

										<div class="control-group">
											<label class="control-label" for="basicinput">Email</label>
											<div class="controls">
												<input data-title="A tooltip for the input" value="{{$staffs->email}}" type="text" placeholder="Enter a Bill number..." data-original-title="" class="span8 tip" name="email">
											</div>
										</div>

											<div class="control-group">
											<label class="control-label" for="basicinput">Temporary Address</label>
											<div class="controls">
												<input data-title="A tooltip for the input" value="{{$staffs->temporary_address}}" type="text" placeholder="Enter a Bill number..." data-original-title="" class="span8 tip" name="temporary_address">
											</div>
										</div>

										<div class="control-group">
											<label class="control-label" for="basicinput">Parement Address</label>
											<div class="controls">
												<input data-title="A tooltip for the input" value="{{$staffs->parement_address}}" type="text" placeholder="Enter a Bill number..." data-original-title="" class="span8 tip" name="parement_address">
											</div>
										</div>


											<div class="control-group">
											<label class="control-label" for="basicinput">Post</label>
											<div class="controls">
												<input data-title="A tooltip for the input" value="{{$staffs->post}}" type="text" placeholder="Enter a Bill number..." data-original-title="" class="span8 tip" name="post">
											</div>
										</div>
								

											<div class="control-group">
											<label class="control-label" for="basicinput">Phone</label>
											<div class="controls">
												<input data-title="A tooltip for the input" value="{{$staffs->phone}}" type="text" placeholder="Enter a Bill number..." data-original-title="" class="span8 tip" name="phone">
											</div>
										</div>
						
										

									

										<div class="control-group">
											<div class="controls">
								{{Form::submit('Create', array('class'=> 'btn btn-success btn-lg'))}}
												<!-- <button type="submit" class="btn">Submit Form</button> -->
											</div>
										</div>
									{!! Form::close() !!} 

							</div>
						</div>

						
						
					</div><!--/.content-->
				</div><!--/.span9-->
			</div>
		</div><!--/.container-->
	</div><!--/.wrapper-->

		@endsection
