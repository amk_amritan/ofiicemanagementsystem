@extends('admins.bill.base')

@section('action-content')
<div class="span9">
					<div class="content">

						<div class="module">
							<div class="module-head">
								<h3>Enter a Bill Detail</h3>
							</div>
							

									<!-- <form class="form-horizontal row-fluid"> -->
										 {!! Form::model($bills ,['route'=>['bill.update',$bills->id], 'class'=>"form-horizontal row-fluid"  ,'method' => 'PUT']) !!}
										<div class="control-group">
											<label class="control-label" for="basicinput">Bill Name</label>
											<div class="controls">
											    <input type="text" id="basicinput" value="{{$bills->bill_name}}" name="bill_name" placeholder="Enter a bill Name" class="span8">
												
											</div>
										</div>

										

										<div class="control-group">
											<label class="control-label" for="basicinput">Bill Number</label>
											<div class="controls">
												<input data-title="A tooltip for the input" type="text" placeholder="Enter a Bill number..." data-original-title="" class="span8 tip" value="{{$bills->bill_number}}" name="bill_number">
											</div>
										</div>

								

										<div class="control-group">
											<label class="control-label" for="basicinput"> Bill Amount</label>
											<div class="controls">
												<div class="input-append">
													<input name="bill_amount" value="{{$bills->bill_amount}}" type="text" placeholder="0000.000" class="span8"><span class="add-on">Rs</span>
												</div>
											</div>
										</div>

							

										<div class="control-group">
											<label class="control-label" for="basicinput">Select Bill Type</label>
											<div class="controls">
												<select name="cr_dr" tabindex="1" data-placeholder="Select here.." class="span8">
													<option value="{{$bills->cr_dr}}">Select here..</option>
													<option value="DR">DR</option>
													<option value="CR">CR</option>
												</select>
											</div>
										</div>


										<div class="control-group">
											<label class="control-label" for="basicinput">Note</label>
											<div class="controls">
												<input type="textarea" name="note"  value="{{$bills->note}}" class="span8" rows="5">
											</div>
										</div>

										<div class="control-group">
											<div class="controls">
								{{Form::submit('Create', array('class'=> 'btn btn-success btn-lg'))}}
												<!-- <button type="submit" class="btn">Submit Form</button> -->
											</div>
										</div>
									{!! Form::close() !!} 

							</div>
						</div>

						
						
					</div><!--/.content-->
				</div><!--/.span9-->
			</div>
		</div><!--/.container-->
	</div><!--/.wrapper-->

		@endsection
