@extends('admins.bill.base')

@section('action-content')
<div class="span9">
					<div class="content">

						<div class="module">
							<div class="module-head">
								<h3>All Bill</h3>
								<a style="padding-left: 700px; text-decoration: none;" href="{{asset('admins/bill/create')}}">Create New Bill</a>
							</div>

							<div class="module-body">
	
				
								<table class="table table-striped">
								  <thead>
									<tr>
									  <th>#</th>
									  <th>Bill Number</th>
									  <th>Bill Amount(Rs)</th>
									  <th>Bill Type</th>
									  <th>Action</th>
									</tr>
								  </thead>
								  <tbody>
								  	@foreach($bill as $bills)
									<tr>
									  <td>{{$bills->id}}</td>
									  <td>{{$bills->bill_number}}</td>
									  <td>{{$bills->bill_amount}}</td>
									  <td>{{$bills->cr_dr}}</td>
									  <td> <a style="text-decoration: none;" href="{{route('bill.edit',$bills->id)}}">Edit</a>  | <a style="text-decoration: none;" href="">Delete</a> </td>
									</tr>
									@endforeach
									
								  </tbody>
								</table>

								
							</div>
						</div><!--/.module-->

					<br />
						
					</div><!--/.content-->
				</div><!--/.span9-->
			</div>
		</div><!--/.container-->
	</div><!--/.wrapper-->
	@endsection