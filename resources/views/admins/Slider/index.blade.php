@extends('admins.slider.base')

@section('action-content')

<div class="row">
	<div class="col-md-10">
		<h3>All Product</h3>
	</div>
	<div class="col-md-2">
		<a href="{{ route('Slider.create') }}" class="btn btn-primary a-btn-slide-text" style="font-size: 15px;"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        <span><strong>Create</strong></span></a>
	</div>
	<hr>

</div>
<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<th>Sn</th>
				<th>Slider Title</th>
				<th>Image</th>
				<th>Action</th>
			</thead>
			<tbody>
				@foreach($slider as $sliders)

				<tr>
					<th>{{$sliders->id}}</th>
					<td>{{$sliders->title}}</td>
					<th>{{$sliders->image}}</th>
					<td><a href="{{route('Slider.show',$sliders->id)}}" style="padding-right: 15px" class="btn btn-primary a-btn-slide-text"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span><span><strong>View</strong></span></a><a href="{{route('Slider.edit',$sliders->id)}}" style="padding-right: 15px" class="btn btn-primary a-btn-slide-text"> <span class="glyphicon glyphicon-edit" aria-hidden="true"></span><span><strong>Edit</strong></span></td>

				</tr>
				@endforeach
			</tbody>
			
		</table>
		
	</div>
</div>

@endsection