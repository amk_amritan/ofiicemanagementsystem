@extends('admins.bill.base')

@section('action-content')
<div class="span9">
					<div class="content">

						<div class="module">
							<div class="module-head">
								<h3>Enter a Bill Detail</h3>
							</div>
							

									<!-- <form class="form-horizontal row-fluid"> -->
										 {!! Form::model($project ,['route'=>['project.update',$project->id], 'class'=>"form-horizontal row-fluid"  ,'method' => 'PUT']) !!}
										<div class="control-group">
											<label class="control-label" for="basicinput">Project Name</label>
											<div class="controls">
											    <input type="text" id="basicinput" value="{{$project->name}}" name="name" placeholder="Enter a bill Name" class="span8">
												
											</div>
										</div>

										

										<div class="control-group">
											<label class="control-label" for="basicinput">Project Id</label>
											<div class="controls">
												<input data-title="A tooltip for the input" type="text" placeholder="Enter a Bill number..." data-original-title="" class="span8 tip" value="{{$project->project_id}}" name="project_id">
											</div>
										</div>

								

										<div class="control-group">
											<label class="control-label" for="basicinput"> Project Amount</label>
											<div class="controls">
												<div class="input-append">
													<input name="price" value="{{$project->price}}" type="text" placeholder="0000.000" class="span8"><span class="add-on">Rs</span>
												</div>
											</div>
										</div>

							

										<div class="control-group">
											<label class="control-label" for="basicinput">Status</label>
											<div class="controls">
												<select name="status" tabindex="1" data-placeholder="Select here.." class="span8">
													<option value="{{$project->status}}">Select here..</option>
													<option value="Complet">Complet</option>
													<option value="Not Start">Not Start</option>
													<option value="Incomplet">Incomplet</option>
												</select>
											</div>
										</div>


										<div class="control-group">
											<label class="control-label" for="basicinput">Note</label>
											<div class="controls">
												<input type="textarea" name="note"  value="{{$project->note}}" class="span8" rows="5">
											</div>
										</div>

										<div class="control-group">
											<div class="controls">
								{{Form::submit('Create', array('class'=> 'btn btn-success btn-lg'))}}
												<!-- <button type="submit" class="btn">Submit Form</button> -->
											</div>
										</div>
									{!! Form::close() !!} 

							</div>
						</div>

						
						
					</div><!--/.content-->
				</div><!--/.span9-->
			</div>
		</div><!--/.container-->
	</div><!--/.wrapper-->

		@endsection
