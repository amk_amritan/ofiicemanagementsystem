@extends('admins.bill.base')

@section('action-content')
<div class="span9">
					<div class="content">

						<div class="module">
							<div class="module-head">
								<h3>All Bill</h3>
								<a style="padding-left: 600px; text-decoration: none;" href="{{asset('admins/project/create')}}">Create New Service/Project</a>
							</div>

							<div class="module-body">
	
				
								<table class="table table-striped">
								  <thead>
									<tr>
									  <th>#</th>
									  <th>Project Number</th>
									  <th>Project Name(Rs)</th>
									  <th>Project Price</th>
									  <th>Action</th>
									</tr>
								  </thead>
								  <tbody>
								  	@foreach($project as $projects)
									<tr>
									  <td>{{$projects->id}}</td>
									  <td>{{$projects->name}}</td>
									  <td>{{$projects->price}}</td>
									  <td>{{$projects->status}}</td>
									  <td> <a style="text-decoration: none;" href="{{route('project.edit',$projects->id)}}">Edit</a>  | <a style="text-decoration: none;" href="">Delete</a> </td>
									</tr>
									@endforeach
									
								  </tbody>
								</table>

								
							</div>
						</div><!--/.module-->

					<br />
						
					</div><!--/.content-->
				</div><!--/.span9-->
			</div>
		</div><!--/.container-->
	</div><!--/.wrapper-->
	@endsection