<!DOCTYPE html>
<html lang="en">
<head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Admin Area</title>
        <link type="text/css" href="{{asset('asset/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <link type="text/css" href="{{asset('asset/bootstrap/css/bootstrap-responsive.min.css')}}" rel="stylesheet">
        <link type="text/css" href="{{asset('asset/css/theme.css')}}" rel="stylesheet">
        <link type="text/css" href="{{asset('asset/images/icons/css/font-awesome.css')}}" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
            rel='stylesheet'>
    </head>
<body>
    <div align="right" style="padding-right: 30px;">
        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout(<?php  echo session()->get('user_name');?>)
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
    </div>

    @include('admins.admin_layouts.sitebar')

    @yield('content')


    
</body>
<script src="{{asset('asset/scripts/jquery-1.9.1.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('asset/scripts/jquery-ui-1.10.1.custom.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('asset/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('asset/scripts/flot/jquery.flot.js')}}" type="text/javascript"></script>
        <script src="{{asset('asset/scripts/flot/jquery.flot.resize.js')}}" type="text/javascript"></script>
        <script src="{{asset('asset/scripts/datatables/jquery.dataTables.js')}}" type="text/javascript"></script>
        <script src="{{asset('asset/scripts/common.js')}}" type="text/javascript"></script>
      
    </body>
</head>