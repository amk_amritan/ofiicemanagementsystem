@extends('frontends.frontend.app')

@section('content')

@include('frontends/frontend_layouts/header')
	<!-- Banner -->
  <div class="slider-content">
	<div class="row">
	<div class="col-sm-3 col-md-3">
		
	</div>
		<div class="col-sm-7 col-md-7">
			<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
						  
						  <div class="carousel-inner">
						    <div class="carousel-item active">
						      <img class="d-block w-100" src="asset/image/Untitled-1.jpg"  alt="First slide">

						    </div>
						    <div class="carousel-item">
						      <img class="d-block w-100" src="asset/image/Untitled-2.jpg"  alt="Second slide">
						    </div>
						    <div class="carousel-item">
						      <img class="d-block w-100" src="asset/image/Untitled-2.jpg"  alt="Third slide">
						    </div>
						    <div class="carousel-item">
						      <img class="d-block w-100" src="asset/image/Untitled-2.jpg" alt="Forth slide">
						    </div>
						  </div>
						  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
						    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
						    <span class="carousel-control-next-icon" aria-hidden="true"></span>
						    <span class="sr-only">Next</span>
						  </a>
					  </div>
		</div>
		<div class="col-sm-2 col-md-2"> 
			<div class="slider-add">
		<img class="d-block w-100" src="asset/image/slider-add.jpg"  alt="First slide" width="100%;">
		</div>
		</div>
		</div>
	</div>
	<div class="content">
		<div class="container">
		<div class="col-md-12 section-title">
                    <h2>SERVICES</h2>
        </div> <!-- /.section -->
        </div>
        <div class="row">
        	<div class="col-sm-12 col-md-12">
        		<div class="services">
        		  @include('frontends/image_carousel')
        		</div>
        	</div>
    	</div>


	</div>

	<div class="content">
		<div class="container">
		<div class="col-md-12 section-title">
                    <h2>top product</h2>
        </div> <!-- /.section -->
        </div>
        <div class="container-fluid">
        	<div class="row">
        		<div class="col-sm-12 col-md-12 col-lg-9">
        			<!-- Top Start hear -->
        			 <section class="newproduct bgwhite p-t-45 p-b-105">
        <div class="row" style="margin-bottom: 50px;">

@foreach($product as $products)
    <div class="col-sm-3 col-md-3"> 
                        <div class="wrap-slick2">
                <div class="slick2">
                    <div class="item-slick2 p-l-15 p-r-15">
                        <div class="block2">
                            <figure class="snip1524">
                                <?php $url=Storage::url('productImage/'.$products->product_image); 
                            
                                ?>
                                <div class="block2-img wrap-pic-w of-hidden pos-relative block2-labelnew">
                               <img src="{{$url}}" alt="IMG-PRODUCT" style="width: 450px; height: 300px" >

                                
                            </div>
                              <figcaption>
                                <div class="block2-overlay trans-0-4">
                                    



                                    <div class="block2-btn-addcart w-size1 trans-0-4">
                                        <!-- Button -->
                                        
                                        <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4" style="border-color:transparent;">
                                            Add to Cart
                                        </button>
                                    </div>
                                    <a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
                                        <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
                                        <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
                                    </a>
                                <p style="margin-top: 50px;">{{$products->product_short_description}}</p>
                                </div>
                              </figcaption>
                              <a href="productDetail"></a>
                            </figure>
                            

                            <div class="block2-txt p-t-20">
                                <a href="productDetail" class="block2-name dis-block s-text3 p-b-5">
                                    {{$products->product_name}}
                                </a>

                                <span class="block2-price m-text6 p-r-5">
                                    ${{$products->product_normal_price}}.00
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        
    </div>
    @endforeach 
</section>



        			<!-- End top product -->
        		</div>
        		<div class="col-sm-12 col-md-12 col-lg-3 " style="padding:4% 4% 4% 4%;">
        			<div class="row">
        				<div class="col-md-6 col-sm-6 col-lg-12">
        				<section class="newproduct bgwhite p-b-105">
						<div class="row">
							<div class="single-sidebar-widget ads-widget">
								<img class="img-fluid" src="asset/image/sidebar-ads2.jpg" alt="">
							</div>
						</div>
					</section>
        			</div>
        			
					<div class="col-md-6 col-sm-6 col-lg-12">
        				<section class="newproduct bgwhite p-b-105">
						<div class="row">
							<div class="single-sidebar-widget ads-widget">
								<img class="img-fluid" src="asset/image/sidebar-ads2.jpg" alt="">
							</div>
						</div>
					</section>
        			</div>
        			</div>
        			
        		</div>
        	</div>
		</div>
	</div>

	<div class="content">
		<div class="container-fluid">

			 <!--Category Product Start  -->

              <div class="row">
        <div class="col-sm-12 col-md-6 ">
            <div class="container">
            <div class="col-md-12 section-title">
                        <h2>Cushions</h2>
            </div> <!-- /.section -->
            </div>


    <section class="newproduct bgwhite p-t-45 p-b-105" style="border-right: 1px solid #dddddd;">
        <div class="row">

            <!-- Loop Start Here -->
            @foreach($Cproduct as $Cproducts)

    <div class="col-sm-6 col-md-12 col-lg-6"> 


    
                    <div class="wrap-slick2">
                <div class="slick2">
                    <div class="item-slick2 p-l-15 p-r-15">
                        <?php $url=Storage::url('productImage/'.$Cproducts->product_image); 
                            
                                ?>
                        <div class="block2">
                            <figure class="snip1524">
                                <div class="block2-img wrap-pic-w of-hidden pos-relative block2-labelnew">
                                <img src="{{$url}}" alt="IMG-PRODUCT" style="width: 400px; height: 250px"">

                                
                            </div>
                              <figcaption>
                                <div class="block2-overlay trans-0-4">
                                    



                                    <div class="block2-btn-addcart w-size1 trans-0-4">
                                        <!-- Button -->
                                        
                                        <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4" style="border-color:transparent;">
                                            Add to Cart
                                        </button>
                                    </div>
                                    <a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
                                        <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
                                        <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
                                    </a>
                                <p style="margin-top: 50px;">{{$Cproducts->product_short_description}} </p>
                                </div>
                              </figcaption>
                              <a href="productDetail"></a>
                            </figure>
                            

                            <div class="block2-txt p-t-20">
                                <a href="productDetail" class="block2-name dis-block s-text3 p-b-5">
                                    {{$Cproducts->product_name}}
                                </a>

                                <span class="block2-price m-text6 p-r-5">
                                    ${{$Cproducts->product_normal_price}}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
    

    </div>
    @endforeach
    <!-- Loop End Here -->

            </div>
        </section>
     </div>



        <div class="col-sm-12 col-md-6">
            <div class="container">
            <div class="col-md-12 section-title">
                        <h2>Bells</h2>
            </div> <!-- /.section -->
            </div>

            <section class="newproduct bgwhite p-t-45 p-b-105">
        <div class="row" >
            <!--Loop Start Here --> 
            @foreach($Coproduct as $Coproducts)
    <div  class="col-sm-6 col-md-12 col-lg-6"> 


    
                    <div class="wrap-slick2">
                <div class="slick2">
                    <div class="item-slick2 p-l-15 p-r-15">
                        <?php $url=Storage::url('productImage/'.$Coproducts->product_image); 
                            
                                ?>
                        <div class="block2">
                            <figure class="snip1524">
                                <div class="block2-img wrap-pic-w of-hidden pos-relative block2-labelnew">
                                <img src="{{$url}}" alt="IMG-PRODUCT" style="width: 400px; height: 250px">

                                
                            </div>
                              <figcaption>
                                <div class="block2-overlay trans-0-4">
                                    



                                    <div class="block2-btn-addcart w-size1 trans-0-4">
                                        <!-- Button -->
                                        
                                        <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4" style="border-color:transparent;">
                                            Add to Cart
                                        </button>
                                    </div>
                                    <a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
                                        <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
                                        <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
                                    </a>
                                <p style="margin-top: 50px;">{{$Coproducts->product_short_description}} </p>
                                </div>
                              </figcaption>
                              <a href="#"></a>
                            </figure>
                            

                            <div class="block2-txt p-t-20">
                                <a href="productDetail.php" class="block2-name dis-block s-text3 p-b-5">
                                    {{$Coproducts->product_name}}
                                </a>

                                <span class="block2-price m-text6 p-r-5">
                                    ${{$Coproducts->product_normal_price}}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
    

    </div>

    <!--Loop End Hear -->

    @endforeach

    
    </div>
</section>


        </div>
    </div>

             <!-- End Category Product   -->
		</div>
	</div>

	<div class="content">
		<div class="container">
		<div class="col-md-12 section-title">
                    <h2>recent product</h2>
        </div> <!-- /.section -->
        </div>
        <div class="container-fluid">
        	<div class="row">
        		<div class="col-sm-12 col-md-12 col-lg-9">
        		 <!-- Recent Product Start -->


                    <section class="newproduct bgwhite p-t-45 p-b-105">
        <div class="row" style="margin-bottom: 50px;">

@foreach($product as $products)
    <div class="col-sm-3 col-md-3"> 
                        <div class="wrap-slick2">
                <div class="slick2">
                    <div class="item-slick2 p-l-15 p-r-15">
                        <div class="block2">
                            <figure class="snip1524">
                                <?php $url=Storage::url('productImage/'.$products->product_image); 
                            
                                ?>
                                <div class="block2-img wrap-pic-w of-hidden pos-relative block2-labelnew">
                                <img src="{{$url}}" alt="IMG-PRODUCT" style="width: 400px; height: 250px" >

                                
                            </div>
                              <figcaption>
                                <div class="block2-overlay trans-0-4">
                                    



                                    <div class="block2-btn-addcart w-size1 trans-0-4">
                                        <!-- Button -->
                                        
                                        <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4" style="border-color:transparent;">
                                            Add to Cart
                                        </button>
                                    </div>
                                    <a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
                                        <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
                                        <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
                                    </a>
                                <p style="margin-top: 50px;">{{$products->product_short_description}}</p>
                                </div>
                              </figcaption>
                              <a href="productDetail"></a>
                            </figure>
                            

                            <div class="block2-txt p-t-20">
                                <a href="productDetail" class="block2-name dis-block s-text3 p-b-5">
                                    {{$products->product_name}}
                                </a>

                                <span class="block2-price m-text6 p-r-5">
                                    ${{$products->product_normal_price}}.00
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        
    </div>
    @endforeach 
</section>

               <!-- End Recent Product -->
        		</div>
        		<div class="col-sm-12 col-md-12 col-lg-3 " style="padding:4% 4% 4% 4%;">
        			<div class="row">
        				<div class="col-md-6 col-sm-6 col-lg-12">
        				<section class="newproduct bgwhite p-b-105">
						<div class="row">
							<div class="single-sidebar-widget ads-widget">
								<img class="img-fluid" src="asset/image/sidebar-ads2.jpg" alt="">
							</div>
						</div>
					</section>
        			</div>
        			
					<div class="col-md-6 col-sm-6 col-lg-12">
        				<section class="newproduct bgwhite p-b-105">
						<div class="row">
							<div class="single-sidebar-widget ads-widget">
								<img class="img-fluid" src="asset/image/sidebar-ads2.jpg" alt="">
							</div>
						</div>
					</section>
        			</div>
        			</div>
        			
        		</div>
        	</div>
		</div>
	</div>


	<div class="content">
		<div class="container-fluid">
			 <!--Category Product Start  -->

              <div class="row">
        <div class="col-sm-12 col-md-6 ">
            <div class="container">
            <div class="col-md-12 section-title">
                        <h2>Mix Product</h2>
            </div> <!-- /.section -->
            </div>


    <section class="newproduct bgwhite p-t-45 p-b-105" style="border-right: 1px solid #dddddd;">
        <div class="row">

            <!-- Loop Start Here -->
            @foreach($Co5product as $Cproducts)

    <div class="col-sm-6 col-md-12 col-lg-6"> 


    
                    <div class="wrap-slick2">
                <div class="slick2">
                    <div class="item-slick2 p-l-15 p-r-15">
                        <?php $url=Storage::url('productImage/'.$Cproducts->product_image); 
                            
                                ?>
                        <div class="block2">
                            <figure class="snip1524">
                                <div class="block2-img wrap-pic-w of-hidden pos-relative block2-labelnew">
                                <img src="{{$url}}" alt="IMG-PRODUCT" style="width: 400px; height: 250px">

                                
                            </div>
                              <figcaption>
                                <div class="block2-overlay trans-0-4">
                                    



                                    <div class="block2-btn-addcart w-size1 trans-0-4">
                                        <!-- Button -->
                                        
                                        <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4" style="border-color:transparent;">
                                            Add to Cart
                                        </button>
                                    </div>
                                    <a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
                                        <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
                                        <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
                                    </a>
                                <p style="margin-top: 50px;">{{$Cproducts->product_short_description}} </p>
                                </div>
                              </figcaption>
                              <a href="productDetail"></a>
                            </figure>
                            

                            <div class="block2-txt p-t-20">
                                <a href="productDetail" class="block2-name dis-block s-text3 p-b-5">
                                    {{$Cproducts->product_name}}
                                </a>

                                <span class="block2-price m-text6 p-r-5">
                                    ${{$Cproducts->product_normal_price}}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
    

    </div>
    @endforeach
    <!-- Loop End Here -->

            </div>
        </section>
     </div>



        <div class="col-sm-12 col-md-6">
            <div class="container">
            <div class="col-md-12 section-title">
                        <h2>Mix Product</h2>
            </div> <!-- /.section -->
            </div>

            <section class="newproduct bgwhite p-t-45 p-b-105">
        <div class="row" >
            <!--Loop Start Here --> 
            @foreach($Co4product as $Coproducts)
    <div  class="col-sm-6 col-md-12 col-lg-6"> 


    
                    <div class="wrap-slick2">
                <div class="slick2">
                    <div class="item-slick2 p-l-15 p-r-15">
                        <?php $url=Storage::url('productImage/'.$Coproducts->product_image); 
                            
                                ?>
                        <div class="block2">
                            <figure class="snip1524">
                                <div class="block2-img wrap-pic-w of-hidden pos-relative block2-labelnew">
                                <img src="{{$url}}" alt="IMG-PRODUCT" style="width: 400px; height: 250px">

                                
                            </div>
                              <figcaption>
                                <div class="block2-overlay trans-0-4">
                                    



                                    <div class="block2-btn-addcart w-size1 trans-0-4">
                                        <!-- Button -->
                                        
                                        <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4" style="border-color:transparent;">
                                            Add to Cart
                                        </button>
                                    </div>
                                    <a href="productDetail" class="block2-btn-addwishlist hov-pointer trans-0-4">
                                        <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
                                        <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
                                    </a>
                                <p style="margin-top: 50px;">{{$Coproducts->product_short_description}} </p>
                                </div>
                              </figcaption>
                              <a href="#"></a>
                            </figure>
                            

                            <div class="block2-txt p-t-20">
                                <a href="productDetail" class="block2-name dis-block s-text3 p-b-5">
                                    {{$Coproducts->product_name}}
                                </a>

                                <span class="block2-price m-text6 p-r-5">
                                    ${{$Coproducts->product_normal_price}}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
    

    </div>

    <!--Loop End Hear -->

    @endforeach

    
    </div>
</section>


        </div>
    </div>

             <!-- End Category Product   -->
		</div>
	</div>
	

	<div class="content">
		<div class="container">
		<div class="col-md-12 section-title">
                    <h2>BOOKS</h2>
        </div> <!-- /.section -->
        </div>
        <div class="container-fluid">
        	<div class="row">
        		<!--Book Display Hear -->
                 @foreach($book as $books)

                    <div class="col-sm-3 col-md-3">
                    <div class="product-item-4">
                        <div class="product-thumb">
                            <img src="asset/image/4.jpg" alt="Product Title">
                        </div> <!-- /.product-thumb -->
                        <div class="product-content overlay">
                            <h5><a href="#">{{$books->book_name}}</a></h5>
                            <span class="tagline">{{$books->short_description}}</span>
                            <span class="price">$60.00</span>
                        </div> <!-- /.product-content -->
                    </div> <!-- /.product-item-4 -->
                    </div>

@endforeach

                 <!--End Book Display  -->


        	</div>
		</div>
	</div>

	<div class="content">
		<div class="container">
		<div class="col-md-12 section-title">
                    <h2>BLOGS</h2>
        </div> <!-- /.section -->
        </div>
        <div class="container-fluid">
        	<div class="row">

        		<!-- Blog Start Hear -->
@foreach($blog as $blogs)


<div class="col-sm-3 col-md-3">
   <div class="blog-card spring-fever" style=" background: url(asset/image/2.jpg) center no-repeat;">
  <div class="title-content">
    <h3><a href="blogDetail">{{$blogs->blog_title}}</a></h3>
  </div>
  <div class="card-info">
    {{$blogs->blog_content}}
    <a href="blogDetail">Read Article<span class="licon icon-arr icon-black"></span></a>
  </div>
  <div class="gradient-overlay"></div>
  <div class="color-overlay"></div>
</div><!-- /.blog-card -->
</div>
@endforeach
</div>



                <!-- End Blog -->
        	</div>
		</div>
	</div>



	
  

  @endsection