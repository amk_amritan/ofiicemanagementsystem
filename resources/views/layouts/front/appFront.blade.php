
<!DOCTYPE HTML>
<html lang="zxx">

<head>
  <title>Creative Wave Information Technology (Office Management Demo)</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta charset="utf-8">
  <meta name="keywords" content="Communal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
  <script>
    addEventListener("load", function () {
      setTimeout(hideURLbar, 0);
    }, false);

    function hideURLbar() {
      window.scrollTo(0, 1);
    }
  </script>

  <!-- Bootstrap Core CSS -->
  <link href="{{asset('assets/css/bootstrap.css')}}" rel='stylesheet' type='text/css' />

  <!-- Custom CSS -->
  <link href="{{asset('assets/css/style.css')}}" rel='stylesheet' type='text/css' />
  <!-- font-awesome icons -->
  <link href="{{asset('assets/css/fontawesome-all.min.css')}}" rel="stylesheet">
  <!-- //Custom Theme files -->

  <!-- side nav css file -->
  <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
  <!-- //side nav css file -->
  <!--webfonts-->
  <!-- logo -->
  <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
  <!-- titles -->
  <link href="//fonts.googleapis.com/css?family=Yanone+Kaffeesatz:200,300,400,700" rel="stylesheet">
  <!-- body -->
  <link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">
  <!--//webfonts-->
</head>


	@yield('content')


<!-- js-->
  <script src="{{asset('assets/js/jquery-2.2.3.min.js')}}"></script>
  <!-- js-->

  <!-- for toggle left push menu script -->
  <script src="{{asset('assets/js/classie.js')}} "></script>
  <script>
    var menuLeft = document.getElementById('cbp-spmenu-s1'),
      showLeftPush = document.getElementById('showLeftPush'),
      body = document.body;

    showLeftPush.onclick = function () {
      classie.toggle(this, 'active');
      classie.toggle(body, 'cbp-spmenu-push-toright');
      classie.toggle(menuLeft, 'cbp-spmenu-open');
      disableOther('showLeftPush');
    };


    function disableOther(button) {
      if (button !== 'showLeftPush') {
        classie.toggle(showLeftPush, 'disabled');
      }
    }
  </script>
  <!-- //Classie -->
  <!-- //for toggle left push menu script -->
  <!-- side nav js -->
  <script src="{{asset('assets/js/SidebarNav.min.js')}}"></script>
  <script>
    $('.sidebar-menu').SidebarNav()
  </script>
  <!-- //side nav js -->
  <!-- script for password match -->
  <script>
    window.onload = function () {
      document.getElementById("password1").onchange = validatePassword;
      document.getElementById("password2").onchange = validatePassword;
    }

    function validatePassword() {
      var pass2 = document.getElementById("password2").value;
      var pass1 = document.getElementById("password1").value;
      if (pass1 != pass2)
        document.getElementById("password2").setCustomValidity("Passwords Don't Match");
      else
        document.getElementById("password2").setCustomValidity('');
      //empty string means no validation error
    }
  </script>
  <!-- script for password match -->
  <!-- start-smooth-scrolling -->
  <script src="{{asset('assets/js/move-top.js')}} "></script>
  <script src="{{asset('assets/js/easing.js')}} "></script>
  <script>
    jQuery(document).ready(function ($) {
      $(".scroll ").click(function (event) {
        event.preventDefault();

        $('html,body').animate({
          scrollTop: $(this.hash).offset().top
        }, 1000);
      });
    });
  </script>
  <!-- //end-smooth-scrolling -->
  <!-- smooth-scrolling-of-move-up -->
  <script>
    $(document).ready(function () {
      /*
       var defaults = {
         containerID: 'toTop', // fading element id
         containerHoverID: 'toTopHover', // fading element hover id
         scrollSpeed: 1200,
         easingType: 'linear' 
       };
       */

      $().UItoTop({
        easingType: 'easeOutQuart'
      });

    });
  </script>
  <script src="{{asset('assets/js/SmoothScroll.min.js')}} "></script>
  <!-- //smooth-scrolling-of-move-up -->
  <script src="{{asset('assets/js/counter.js')}}"></script>
  <!-- //stats -->
  <!-- Bootstrap Core JavaScript -->
  <script src="{{asset('assets/js/bootstrap.js')}}">
  </script>
  <!-- //Bootstrap Core JavaScript -->

</body>

</html>