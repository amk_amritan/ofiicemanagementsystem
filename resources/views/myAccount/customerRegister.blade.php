@extends('layouts.front.appFrontSec')



@section('content')
	 <style type="text/css">
        .hide{display:none;}
        .btn {
        display: inline-block;
        vertical-align: middle;
        cursor: pointer;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        }

    </style>
<section class="m50 ">
    <div class="container">
        <div class="row justify-content-center mt100">
            <div class="col-md-6 col-12" >
              
                <div class="contact_info">
                    <h5 style="text-align:center;">
                        Register For New Account
                    </h5>
                    <div class="contact_form">
                    <form action="{{url('/customerRegister/registerCustomer')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                    <div class="form-group" style="margin-left: 40%;">
                        <input type="file" name="image" id="imageUpload" class="hide"/> 
                         
                        <label for="imageUpload" class=""><img src="./assets/image/user.png" style="height:100px;width:100px;border-radius:50px; border: 1px solid #A9240F;" id="imagePreview" alt="Preview Image" width="200px"/><br>Profile Picture</label>

                    </div>
                    <div class="row">
                        <div class="form-group col-md-6 "> 
                            <div class="form-group">
                                <label>First Name</label>
                                <input name="fname" type="text" class="form-control" placeholder="Enter Your Name" required>
                            </div>
                        </div>
                        <div class="form-group col-md-6 "> 
                            <div class="form-group">
                                <label>Last Name</label>
                                <input  name="lname" type="text" class="form-control" placeholder="Enter Your Name" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Enter Your Email</label>
                        <input name="email" type="email" class="form-control" placeholder="Enter Your Email" required>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6 ">
                            <label>Country</label>
                              <select name="country" class="form-control" style="height:50%;">
                                        <option value="null">District: </option>
                                        <option value="Achham">Achham</option>
                                        <option value="Arghakhanchi">Arghakhanchi</option>
                                        <option value="Baglung">Baglung</option>
                                        <option value="Baitadi">Baitadi</option>
                                        <option value="Bajhang">Bajhang</option>
                                        <option value="Bajura">Bajura</option>
                                        <option value="Banke">Banke</option>
                                        <option value="Bara">Bara</option>
                                        <option value="Bardiya">Bardiya</option>
                                        <option value="Bhaktapur">Bhaktapur</option>
                                        <option value="Bhojpur">Bhojpur</option>
                                        <option value="Chitwan">Chitwan</option>
                                        <option value="Dadeldhura">Dadeldhura</option>
                                        <option value="Dailekh">Dailekh</option>
                                        <option value="Dang deukhuri">Dang deukhuri</option>
                                        <option value="Darchula">Darchula</option>
                                        <option value="Dhading">Dhading</option>
                                        <option value="Dhankuta">Dhankuta</option>
                                        <option value="Dhanusa">Dhanusa</option>
                                        <option value="Dholkha">Dholkha</option>
                                        <option value="Dolpa">Dolpa</option>
                                        <option value="Doti">Doti</option>
                                        <option value="Gorkha">Gorkha</option>
                                        <option value="Gulmi">Gulmi</option>
                                        <option value="Humla">Humla</option>
                                        <option value="Ilam">Ilam</option>
                                        <option value="Jajarkot">Jajarkot</option>
                                        <option value="Jhapa">Jhapa</option>
                                        <option value="Jumla">Jumla</option>
                                        <option value="Kailali">Kailali</option>
                                        <option value="Kalikot">Kalikot</option>
                                        <option value="Kanchanpur">Kanchanpur</option>
                                        <option value="Kapilvastu">Kapilvastu</option>
                                        <option value="Kaski">Kaski</option>
                                        <option value="Kathmandu">Kathmandu</option>
                                        <option value="Kavrepalanchok">Kavrepalanchok</option>
                                        <option value="Khotang">Khotang</option>
                                        <option value="Lalitpur">Lalitpur</option>
                                        <option value="Lamjung">Lamjung</option>
                                        <option value="Mahottari">Mahottari</option>
                                        <option value="Makwanpur">Makwanpur</option>
                                        <option value="Manang">Manang</option>
                                        <option value="Morang">Morang</option>
                                        <option value="Mugu">Mugu</option>
                                        <option value="Mustang">Mustang</option>
                                        <option value="Myagdi">Myagdi</option>
                                        <option value="Nawalparasi">Nawalparasi</option>
                                        <option value="Nuwakot">Nuwakot</option>
                                        <option value="Okhaldhunga">Okhaldhunga</option>
                                        <option value="Palpa">Palpa</option>
                                        <option value="Panchthar">Panchthar</option>
                                        <option value="Parbat">Parbat</option>
                                        <option value="Parsa">Parsa</option>
                                        <option value="Pyuthan">Pyuthan</option>
                                        <option value="Ramechhap">Ramechhap</option>
                                        <option value="Rasuwa">Rasuwa</option>
                                        <option value="Rautahat">Rautahat</option>
                                        <option value="Rolpa">Rolpa</option>
                                        <option value="Rukum">Rukum</option>
                                        <option value="Rupandehi">Rupandehi</option>
                                        <option value="Salyan">Salyan</option>
                                        <option value="Sankhuwasabha">Sankhuwasabha</option>
                                        <option value="Saptari">Saptari</option>
                                        <option value="Sarlahi">Sarlahi</option>
                                        <option value="Sindhuli">Sindhuli</option>
                                        <option value="Sindhupalchok">Sindhupalchok</option>
                                        <option value="Siraha">Siraha</option>
                                        <option value="Solukhumbu">Solukhumbu</option>
                                        <option value="Sunsari">Sunsari</option>
                                        <option value="Surkhet">Surkhet</option>
                                        <option value="Syangja">Syangja</option>
                                        <option value="Tanahu">Tanahu</option>
                                        <option value="Taplejung">Taplejung</option>
                                        <option value="Terhathum">Terhathum</option>
                                        <option value="Udayapur">Udayapur</option>
                              </select>
                        </div>
                        <div class="form-group col-md-6 ">
                            <label>City</label>
                            <input type="text" name="city" class="form-control" style="height:50%;" placeholder="Enter Your City" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Contact Number</label>
                        <input type="phone" name="ContactNumber" class="form-control" placeholder="Enter Your Contact Number" required>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control" placeholder="Enter Your Password" required>
                    </div>
                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input type="password" name="cpassword" class="form-control" placeholder="Confirm Your Password" required>
                    </div>
                    <div class="checkbox icheck">
                        <input type="checkbox" id="myCheck" name="checkbox" value="1" checked="checked">
                        <label for="condition"> Accept the terms and condtions</label>
                        
                      </div>
                    <div class="form-group text-right">
                        <button type="Submit" name="submit" class="primary-btn">Submit</button>
                    </div>
                <form>
                </div>
                </div>
            </div>
            <div class="col-md-6 col-12" >
                <div class="contact_info " style="margin-top: 13%;">
                    
                </div>
            </div>
        </div>
    </section>

 @endsection
    
	
