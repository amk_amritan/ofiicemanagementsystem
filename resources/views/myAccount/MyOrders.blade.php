

@extends('layouts.front.appFrontSec')



@section('content')

<div class="container-fluid category_content">
        <!-- /.row -->
        <div class="row">
            <div class="col-md-3">
                <div>
                    <a href="#" class="list-group-item active">My Account
                    </a>
                    <ul class="list-group">

                        <a href="{{asset('customerLogin')}}"><li class="list-group-item" style="color: black;"> Important Details
                        </li></a>
                        <a href="{{asset('personalInformation')}}"><li class="list-group-item" style="color: black;">Personal Information
                        </li></a>
                        <a href="{{asset('ratings')}}"><li class="list-group-item" style="color: black;">My Reviews & Ratings
                        </li></a>
                        <a href="{{asset('customerOrders')}}"><li class="list-group-item">My Orders
                        </li></a>
                        
                    </ul>
                </div>
               <div style="margin-top:8px;">
               	<div class="single-sidebar-widget ads-widget">
					<img class="img-fluid" src="assets/image/sidebar-ads.jpg" alt="">
				</div>
               </div>


              
                <!-- /.div -->
               
            </div>
            <!-- /.col -->
            <div class="col-md-9">

            	<div>
                    <div class="section-title">
                    <h2>My Orders</h2>
        			</div> <!-- /.section -->
                </div>
                

                <div>
                    <table id="example" class="shopping-cart-table table " style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th></th>
                                        <th class="text-center">Price</th>
                                        <th class="text-center">Quantity</th>
                                        <th class="text-center">Total</th>
                                        <th class="text-right"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="thumb"><img style="height:60px;" src="./assets/image/product07.jpg" alt=""></td>
                                        <td class="details">
                                            <a href="#">Product Name Goes Here</a>
                                            <ul>
                                                <li><span>Size: XL</span></li>
                                                <li><span>Color: Camelot</span></li>
                                            </ul>
                                        </td>
                                        <td class="price text-center"><strong>$32.50</strong><br><del class="font-weak"><small>$40.00</small></del></td>
                                        <td class=" text-center"><input class="qty input" type="number" value="1" readonly></td>
                                        <td class="total text-center"><strong class="primary-color">$32.50</strong></td>
                                        <td class="text-right"><button class="main-btn icon-btn"><i class="fa fa-trash"></i></button></td>
                                    </tr>
                                    <tr>
                                        <td class="thumb"><img style="height:60px;" src="./assets/image/product07.jpg" alt=""></td>
                                        <td class="details">
                                            <a href="#">Product Name Goes Here</a>
                                            <ul>
                                                <li><span>Size: XL</span></li>
                                                <li><span>Color: Camelot</span></li>
                                            </ul>
                                        </td>
                                        <td class="price text-center"><strong>$32.50</strong></td>
                                        <td class=" text-center"><input class="qty input" type="number" value="1" readonly></td>
                                        <td class="total text-center"><strong class="primary-color">$32.50</strong></td>
                                        <td class="text-right"><button class="main-btn icon-btn"><i class="fa fa-trash"></i></button></td>
                                    </tr>
                                    <tr>
                                        <td class="thumb"><img style="height:60px;" src="./assets/image/product07.jpg" alt=""></td>
                                        <td class="details">
                                            <a href="#">Product Name Goes Here</a>
                                            <ul>
                                                <li><span>Size: XL</span></li>
                                                <li><span>Color: Camelot</span></li>
                                            </ul>
                                        </td>
                                        <td class="price text-center"><strong>$32.50</strong></td>
                                        <td class=" text-center"><input class="qty input" type="number" value="1" readonly></td>
                                        <td class="total text-center"><strong class="primary-color">$32.50</strong></td>
                                        <td class="text-right"><button class="main-btn icon-btn"><i class="fa fa-trash"></i></button></td>
                                    </tr>
                                </tbody>
                            </table>
                </div>
                
               
                
                <!-- /.row -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->


     @endsection

