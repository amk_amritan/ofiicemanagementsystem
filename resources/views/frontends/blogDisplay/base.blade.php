@extends('frontends.frontend.app')

@include('frontends/header')
@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    @yield('action-content')
    <!-- /.content -->
  </div>
@endsection