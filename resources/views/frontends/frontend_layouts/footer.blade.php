
	<!-- Footer -->
	<footer class="footer">
		<div class="container">
			<div class="row">

				<div class="col-lg-3 footer_col">
					<div class="footer_column footer_contact">
						<div class="logo_container">
							<div class="logo"><a href="#"> NSH internationl School</a></div>
						</div>
						<div class="footer_title">Got Question? Call Us 24/7</div>
						<div class="footer_phone">+977- 9849437904</div>
						<div class="footer_contact_text">
							<p>Nayabazer, Kathmandu</p>
							<p>Wrad No - 16</p>
						</div>
						<div class="footer_social">
							<ul>
								<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
								<li><a href="#"><i class="fab fa-twitter"></i></a></li>
								<li><a href="#"><i class="fab fa-youtube"></i></a></li>
								<li><a href="#"><i class="fab fa-google"></i></a></li>
								<li><a href="#"><i class="fab fa-vimeo-v"></i></a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-lg-2 offset-lg-3">
					<div class="footer_column">
						<div class="footer_title">Quick Menu</div>
						<ul class="footer_list">
							@foreach($category as $categorys)
							<li>-> <a href="#">{{$categorys->category_name}}</a></li>
							@endforeach
						
						</ul>
					
					</div>
				</div>


				<div class="col-lg-2 col-sm-2 col-md-2">
					<div class="footer_column">
						<div class="footer_title">Our Branch</div>
						<ul class="footer_list">
							<li><a href="#">Kathmandu Branch</a></li>
							<li><a href="#">Pokhara Branch</a></li>
							<li><a href="#">Biratnager Branch</a></li>
							<li><a href="#">Ithari Branch </a></li>
							<li><a href="#">Khandbari Branch</a></li>
						</ul>
						
					</div>
				</div>


				<div class="col-lg-2 col-sm-2 col-md-2">
					<div class="footer_column">
						<div class="footer_title">Account Info</div>
						<ul class="footer_list">
							<li><a href="register">Sign Up</a></li>
							<li><a href="login">Login</a></li>
							<li><a href="#">My Account</a></li>
							<li><a href="mycart">Cart</a></li>
							<li><a href="blog">Blog</a></li>
						</ul>
					</div>
				</div>

				

			</div>
		</div>
	</footer>

	<!-- payments -->

	<div class="copyright" style="background-color: #D63218">
		<div class="container" >
			<div class="row" >
				<div class="col-lg-3">
					
				</div>
				<div class="col-lg-6" align="center" >
					<div class="payment_title" >Payment Methods</div>
						<img src="asset/image/visa.jpg" alt="" style="height: 40px; width: 100px;">
						<img src="asset/image/paypal.jpg" alt="" style="height: 40px; width: 100px;">
						<img src="asset/image/mastercard.jpg" alt="" style="height: 40px; width: 100px;">
						
				</div>
				<div class="col-lg-3"></div>
			</div>
		</div>
	</div>



	<!-- Copyright -->

	<div class="copyright">
		<div class="container">
			<div class="row">
				<div class="col-lg-4"></div>
				<div class="col-lg-4">
					
					<div class="copyright_container d-flex flex-sm-row flex-column align-items-center justify-content-start">
						<div class="copyright_content" align="center">
						<a>Copyright &copy; All rights reserved | This template is made by Me</a>
						</div>
						
					</div>
				</div>
				<div class="col-lg-4"></div>
			</div>
		</div>
	</div>
</div>