<!DOCTYPE html>
<html lang="en">
<head>
<title>Online Store</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Online Store">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="asset/bootstrap4/bootstrap.min.css">
<link href="asset/fonts/fontawesome-free-5.0.1/css/fontawesome-all.css" rel="stylesheet" type="text/css">
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="asset/fonts/elegant-font/html-css/style.css">
<link rel="stylesheet" type="text/css" href="asset/styles/main_styles.css">
<link rel="stylesheet" type="text/css" href="asset/styles/responsive.css">
<link rel="stylesheet" type="text/css" href="asset/css/mainpage.css">
<link rel="stylesheet" type="text/css" media="all" href="asset/styles/owl.carousel.css">
<link rel="stylesheet" type="text/css" media="all" href="asset/styles/owl.theme.css">
<link rel="stylesheet" type="text/css" href="asset/css/util.css">
<style type="text/css">
  .image-slider {
    margin-left: 6px;
  min-width: 100%;
  min-height: 50px;
  display: flex;
  overflow-x: none;
}
.image-slider::-webkit-scrollbar {
  display: none;
}
.card--content {  min-width: 1460px;
  margin: 5px;
}

</style>

</head>

<body>
	<div class="super_container">



    @yield('content')

  @include('frontends.frontend_layouts.footer')



	</div>
</body>
 <script src="asset/js/jquery.min.js"></script>
<script src="asset/bootstrap4/popper.js"></script>
<script src="asset/bootstrap4/bootstrap.min.js"></script>
<script src="asset/greensock/TweenMax.min.js"></script>
<script type="text/javascript" src="asset/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="asset/js/main.js"></script>
 <script src="asset/ItemSlider/js/modernizr.custom.63321.js"></script>
 <script type="text/javascript" src="asset/js/smoothproducts.min.js"></script>
 <script src="asset/ItemSlider/js/jquery.catslider.js"></script>
 <script src="asset/js/baguetteBox.min.js"></script>
 <script type="text/javascript" src="asset/DataTables/datatables.min.js"></script>
  <script type="text/javascript" src="asset/DataTables/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
 
  $("#owl-demo").owlCarousel({
    navigation : true
  });
 
});

baguetteBox.run('.tz-gallery');

</script>
<script type="text/javascript">
         $('#imageUpload').change(function(){            
            readImgUrlAndPreview(this);
            function readImgUrlAndPreview(input){
                 if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {                          
                            $('#imagePreview').attr('src', e.target.result);
                            }
                      };
                      reader.readAsDataURL(input.files[0]);
                 }  
        });

        $(function () {

            $('#mi-slider').catslider();

        });
    </script>

    <script type="text/javascript">
    $(window).load(function() {
      $('.sp-wrap').smoothproducts();
    });
   
     
</script> 
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
</html>