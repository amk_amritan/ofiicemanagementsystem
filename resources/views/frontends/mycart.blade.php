 @extends('frontends.frontend.app')

@section('content')
@include('frontends/header')

	<div class="container">
        <section class="m50 row ">
        <div class="col-md-12">
                        <div class="order-summary clearfix">
                            <div class="section-title">
                                <h3 class="title">Order Review</h3>
                            </div>
                            <table class="shopping-cart-table table">
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th></th>
                                        <th class="text-center">Price</th>
                                        <th class="text-center">Quantity</th>
                                        <th class="text-center">Total</th>
                                        <th class="text-right"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="thumb"><img style="height:60px;" src="./assets/image/product07.jpg" alt=""></td>
                                        <td class="details">
                                            <a href="#">Product Name Goes Here</a>
                                            <ul>
                                                <li><span>Size: XL</span></li>
                                                <li><span>Color: Camelot</span></li>
                                            </ul>
                                        </td>
                                        <td class="price text-center"><strong>$32.50</strong><br><del class="font-weak"><small>$40.00</small></del></td>
                                        <td class=" text-center"><input class="qty input" type="number" value="1"></td>
                                        <td class="total text-center"><strong class="primary-color">$32.50</strong></td>
                                        <td class="text-right"><button class="main-btn icon-btn"><i class="fa fa-trash"></i></button></td>
                                    </tr>
                                    <tr>
                                        <td class="thumb"><img style="height:60px;" src="./assets/image/product07.jpg" alt=""></td>
                                        <td class="details">
                                            <a href="#">Product Name Goes Here</a>
                                            <ul>
                                                <li><span>Size: XL</span></li>
                                                <li><span>Color: Camelot</span></li>
                                            </ul>
                                        </td>
                                        <td class="price text-center"><strong>$32.50</strong></td>
                                        <td class=" text-center"><input class="qty input" type="number" value="1"></td>
                                        <td class="total text-center"><strong class="primary-color">$32.50</strong></td>
                                        <td class="text-right"><button class="main-btn icon-btn"><i class="fa fa-trash"></i></button></td>
                                    </tr>
                                    <tr>
                                        <td class="thumb"><img style="height:60px;" src="./assets/image/product07.jpg" alt=""></td>
                                        <td class="details">
                                            <a href="#">Product Name Goes Here</a>
                                            <ul>
                                                <li><span>Size: XL</span></li>
                                                <li><span>Color: Camelot</span></li>
                                            </ul>
                                        </td>
                                        <td class="price text-center"><strong>$32.50</strong></td>
                                        <td class=" text-center"><input class="qty input" type="number" value="1"></td>
                                        <td class="total text-center"><strong class="primary-color">$32.50</strong></td>
                                        <td class="text-right"><button class="main-btn icon-btn"><i class="fa fa-trash"></i></button></td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th class="empty" colspan="3"></th>
                                        <th>SUBTOTAL</th>
                                        <th colspan="2" class="sub-total">$97.50</th>
                                    </tr>
                                    <tr>
                                        <th class="empty" colspan="3"></th>
                                        <th>SHIPING</th>
                                        <td colspan="2">Free Shipping</td>
                                    </tr>
                                    <tr>
                                        <th class="empty" colspan="3"></th>
                                        <th>TOTAL</th>
                                        <th colspan="2" class="total">$97.50</th>
                                    </tr>
                                </tfoot>
                            </table>
                            <div align="right">
                                <button class="primary-btn">Place Order</button>
                            </div>
                        </div>

                    </div>
    </section>
    </div>
	
@endsection