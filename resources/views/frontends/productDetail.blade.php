 @extends('frontends.frontend.app')

@section('content')
@include('frontends/header')




        <div class="container-fluid content">
        
        <!-- /.row -->
        <div class="row">
            <div class="col-md-3">
                <div>
                    <a href="#" class="list-group-item active">Categories
                    </a>
                    <ul class="list-group">

                        <a href="#"><li class="list-group-item">Mobile
                        </li></a>
                        <a href="#"><li class="list-group-item">Computers
                        </li></a>
                        <a href="#"><li class="list-group-item">Tablets
                        </li></a>
                        <a href="#"><li class="list-group-item">Appliances
                        </li></a>
                        <a href="#"><li class="list-group-item">Games & Entertainment
                        </li></a>
                         <a href="#"><li class="list-group-item">Mobile
                        </li></a>
                        <a href="#"><li class="list-group-item">Computers
                        </li></a>
                        <a href="#"><li class="list-group-item">Tablets
                        </li></a>
                        <a href="#"><li class="list-group-item">Appliances
                        </li></a>
                        <a href="#"><li class="list-group-item">Games & Entertainment
                        </li></a>
                    </ul>
                </div>
               <div style="margin-top:8px;">
               	<div class="single-sidebar-widget ads-widget">
					<img class="img-fluid" src="asset/image/sidebar-ads.jpg" alt="">
				</div>
               </div>

              
                <!-- /.div -->
               
            </div>

            <div class="col-md-8">

		         <div class="card">
        <div class="wrapper row">
          <div class="col-md-1"></div>
          <div class="preview col-md-5">
            
    <div class="sp-wrap">
      <a href="./asset/image/item-01.jpg"><img src="./asset/image/item-01.jpg" alt=""></a>
      <a href="./asset/image/item-01.jpg"><img src="./asset/image/item-01.jpg" alt=""></a>
    </div>
            
          </div>
         <div class="col-sm-6 col-md-6 col-lg-6">
        <!-- product -->
      <div class="product-content clearfix product-deatil">
          
            <div class="name">
              <blockquote class="blockquote">
             <h2> Product Title</h2>
                <span class="fa fa-star checked"></span>
                  <span class="fa fa-star checked"></span>
                  <span class="fa fa-star checked"></span>
                  <span class="fa fa-star"></span>
                  <span class="fa fa-star"></span>
          
              <small>Average Rating : 4</small>
                </blockquote>       
            </div>
           
            <hr>
            
            <h3 class="price-container">
             <strike> $225</strike><br>
             $200
              <small>*tax included</small>
            </h3>
            <hr>
            <form class="form-horizontal" enctype="multipart/form-data" action="" method="post">
                    <label class="control-label" style="font-size: 20px;">Quantity</label>
                    <input id="numberincre" type="number" name="number" class="form-control text-center" value="1" style="max-width: 40%;">
                 
                   
                    <div align="right" style="padding: 5%;"> 
                     <button type="submit" name="btnsave" class="btn btn-lg btn-primary">Add To Cart
                      </button>
                      <button type="submit" name="btnsave" class="btn btn-lg btn-primary">
                        <div class="wishlist_icon"><img src="asset/image/whiteheart.png" alt=""></div>
                      </button>
                    </div>
                </form>
                <hr>
         
                
          </div>
      </div>
      <!-- end product -->
    </div>
    <div class="row">
      <div class="col-md-12">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Product Detail</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Allergy</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Review</a>
  </li>
</ul>
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
          <div class="advert_title"><a href="#">Trends 2018</a></div>
          <div class="advert_text">Lorem ipsum dolor sit amet, consectetur adipiscing Donec et.</div>
  </div>
  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
        <div class="advert_title"><a href="#">Trends 2018</a></div>
          <div class="advert_text">Lorem ipsum dolor sit amet, consectetur adipiscing Donec et.</div>
  </div>
  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
        <div class="well well-sm">
            <div class="text-right">
                <a class="btn  btn-primary" href="#reviews-anchor" id="open-review-box">Leave a Review</a>
            </div>
        
            <div class="row" id="post-review-box" style="display:none; max-width: 70%;">
                <div class="col-md-12">
                    <form accept-charset="UTF-8" action="" method="post">
                              <div align="right" style="padding-bottom: 5px;">
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                              </div>
                        <textarea class="form-control animated" cols="50" id="new-review" name="comment" placeholder="Enter your review here..." rows="5"></textarea>
        
                        <div class="text-right">
                            <div class="stars star" data-rating="0"></div>
                            <a class="btn btn-md btn-danger" href="#" id="close-review-box" >
                            Cancel</a>
                            <button class="btn btn-md btn-primary" type="submit">Save</button>
                        </div>
                    </form>

                   
                </div>
            </div>
        </div>

        <div class="review-block">
          <div class="row">
            <div class="col-sm-4">
              <img src="http://dummyimage.com/60x60/666/ffffff&text=No+Image" class="img-rounded">
              <div class="review-block-name"><a href="#">Aakash Karkee</a></div>
              <div class="review-block-date">January 29, 2016<br/>1 day ago</div>
            </div>
            <div class="col-sm-8">
              <div class="review-block-rate">
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star"></span>
                <span class="fa fa-star"></span>
              </div>
              <div class="review-block-description">this was nice in buy. this was nice in buy. </div>
            </div>
          </div>
          <hr/> 
          <div class="row">
            <div class="col-sm-4">
              <img src="http://dummyimage.com/60x60/666/ffffff&text=No+Image" class="img-rounded">
              <div class="review-block-name"><a href="#">Aakash Karkee</a></div>
              <div class="review-block-date">January 29, 2016<br/>1 day ago</div>
            </div>
            <div class="col-sm-8">
              <div class="review-block-rate">
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star"></span>
                <span class="fa fa-star"></span>
              </div>
              <div class="review-block-description">this was nice in buy. this was nice in buy. </div>
            </div>
          </div>
        </div>

</div>
      </div>
    </div>
     </div>
            <!-- /.col -->
            
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
        


	</div>
</div>
<div class="container">
  <div class="row">
    <div class="col-md-12 section-title">
                    <h2>related products</h2>
        </div> <!-- /.section -->
<section class="newproduct bgwhite p-t-45 p-b-105">
    <div class="row">
  <div class="col-sm-3 col-md-3"> 


  
          <div class="wrap-slick2">
        <div class="slick2">
          <div class="item-slick2 p-l-15 p-r-15">
            <div class="block2">
              <figure class="snip1524">
                <div class="block2-img wrap-pic-w of-hidden pos-relative block2-labelnew">
                <img src="./asset/image/item-01.jpg" alt="IMG-PRODUCT">

                
              </div>
                <figcaption>
                  <div class="block2-overlay trans-0-4">
                  



                  <div class="block2-btn-addcart w-size1 trans-0-4">
                    <!-- Button -->
                    
                    <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4" style="border-color:transparent;">
                      Add to Cart
                    </button>
                  </div>
                  <a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
                    <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
                    <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
                  </a>
                  <p style="margin-top: 50px;">The only skills I have the patience to learn are those that have no real application in life. </p>
                </div>
                </figcaption>
                <a href="#"></a>
              </figure>
              

              <div class="block2-txt p-t-20">
                <a href="productDetail.php" class="block2-name dis-block s-text3 p-b-5">
                  Herschel supply co 25l
                </a>

                <span class="block2-price m-text6 p-r-5">
                  $75.00
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
      
  

  </div>

  <div class="col-sm-3 col-md-3"> 
          <div class="wrap-slick2">
        <div class="slick2">
          <div class="item-slick2 p-l-15 p-r-15">
            <div class="block2">
              <figure class="snip1524">
                <div class="block2-img wrap-pic-w of-hidden pos-relative block2-labelnew">
                <img src="./asset/image/item-01.jpg" alt="IMG-PRODUCT">

                
              </div>
                <figcaption>
                  <div class="block2-overlay trans-0-4">
                  



                  <div class="block2-btn-addcart w-size1 trans-0-4">
                    <!-- Button -->
                    
                    <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4" style="border-color:transparent;">
                      Add to Cart
                    </button>
                  </div>
                  <a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
                    <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
                    <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
                  </a>
                  <p style="margin-top: 50px;">The only skills I have the patience to learn are those that have no real application in life. </p>
                </div>
                </figcaption>
                <a href="#"></a>
              </figure>
              

              <div class="block2-txt p-t-20">
                <a href="productDetail.php" class="block2-name dis-block s-text3 p-b-5">
                  Herschel supply co 25l
                </a>

                <span class="block2-price m-text6 p-r-5">
                  $75.00
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
      
    
  </div>

  <div class="col-sm-3 col-md-3"> 
          <div class="wrap-slick2">
        <div class="slick2">
          <div class="item-slick2 p-l-15 p-r-15">
            <div class="block2">
              <figure class="snip1524">
                <div class="block2-img wrap-pic-w of-hidden pos-relative block2-labelnew">
                <img src="./asset/image/item-01.jpg" alt="IMG-PRODUCT">

                
              </div>
                <figcaption>
                  <div class="block2-overlay trans-0-4">
                  



                  <div class="block2-btn-addcart w-size1 trans-0-4">
                    <!-- Button -->
                    
                    <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4" style="border-color:transparent;">
                      Add to Cart
                    </button>
                  </div>
                  <a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
                    <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
                    <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
                  </a>
                  <p style="margin-top: 50px;">The only skills I have the patience to learn are those that have no real application in life. </p>
                </div>
                </figcaption>
                <a href="#"></a>
              </figure>
              

              <div class="block2-txt p-t-20">
                <a href="productDetail.php" class="block2-name dis-block s-text3 p-b-5">
                  Herschel supply co 25l
                </a>

                <span class="block2-price m-text6 p-r-5">
                  $75.00
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
      
    
  </div>

  <div class="col-sm-3 col-md-3"> 
          <div class="wrap-slick2">
        <div class="slick2">
          <div class="item-slick2 p-l-15 p-r-15">
            <div class="block2">
              <figure class="snip1524">
                <div class="block2-img wrap-pic-w of-hidden pos-relative block2-labelnew">
                <img src="./asset/image/item-01.jpg" alt="IMG-PRODUCT">

                
              </div>
                <figcaption>
                  <div class="block2-overlay trans-0-4">
                  



                  <div class="block2-btn-addcart w-size1 trans-0-4">
                    <!-- Button -->
                    
                    <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4" style="border-color:transparent;">
                      Add to Cart
                    </button>
                  </div>
                  <a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
                    <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
                    <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
                  </a>
                  <p style="margin-top: 50px;">The only skills I have the patience to learn are those that have no real application in life. </p>
                </div>
                </figcaption>
                <a href="#"></a>
              </figure>
              

              <div class="block2-txt p-t-20">
                <a href="productDetail.php" class="block2-name dis-block s-text3 p-b-5">
                  Herschel supply co 25l
                </a>

                <span class="block2-price m-text6 p-r-5">
                  $75.00
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
      
    
  </div>
  </div>
</section>

                    <!-- /.col -->
</div>
</div>




@endsection